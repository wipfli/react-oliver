import React from "react";
import ReactMapGL from "react-map-gl";
import { fromJS } from "immutable";
import MAP_STYLE from "./../style.json";

const defaultMapStyle = fromJS(MAP_STYLE);

export default class App extends React.Component {
  render() {
    return <Map />;
  }
}

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        width: 400,
        height: 400,
        latitude: 47.372,
        longitude: 8.542,
        zoom: 8
      }
    };
  }

  componentDidMount() {
    console.log(MAP_STYLE);
  }

  render() {
    return (
      <ReactMapGL
        mapStyle={defaultMapStyle}
        {...this.state.viewport}
        onViewportChange={viewport => this.setState({ viewport })}
      />
    );
  }
}
